package tests

import (
	"git.championtek.com.tw/go/logger/v2"
	"github.com/olivere/elastic/v7"
)

func getConfig() logger.Config  {
	elkConfig := logger.Config{
		URL: "http://54.248.41.134",
		Port: "9200",
		Type: 1,
		BasicAuthUser: "elastic",
		BasicAuthPassword: "work@vmp6jo4",
		Index: "logger_unit_test",
		NumberOfShards: 1,
		NumberOfReplicas: 0,
	}
	return elkConfig
}

//define variables for testing
func newClient() (*elastic.Client, error)  {
	elkConfig := getConfig()
	if err := logger.Mgr.Init(&elkConfig); err != nil {
		return nil, err
	}
	return logger.Mgr.Client(), nil
}