package tests

import (
	"testing"
	"time"

	"git.championtek.com.tw/go/logger/v2"
)

func TestLoggerInit(t *testing.T) {
	if _, err := newClient(); err != nil {
		t.Error(err)
	}
}

func TestLoggerPut(t *testing.T) {
	_, err := newClient()
	if err != nil {
		t.Error(err)
	}
	//index a logger (using JSON serialization)
	var tags = []string{"減糖", "高纖", "低脂", "鐵", "鋅"}
	if logger.Mgr.Type() == logger.RESTful {
		logData := logger.RESTfulService{
			Service: "unit_test_for_logger",
			IP:      "192.168.1.21",
			Status:  "200 Ok",
			Method:  "Get",
			Path:    "/api/v1/get/",
			Created: time.Now(),
			Tags:    tags,
			Remark:  "",
		}
		if err := logger.Mgr.PutLog(&logData); err != nil {
			t.Error(err)
		}
	} else {
		logData := logger.GraphqlService{
			IP:      "192.168.1.21",
			Request: "POST /service/v1/query",
			Query: `query GetAlternativeDishes($id: Int, $memberId: String) {
  AlternativeDishes(id: $id, memberId: $memberId) {
    brand
    dishName
    calories
    carbohydrate
    protein
    fats
  }
}
`,
			Variables: `{
  "id": 10,
  "memberId": "bf836d1f-7b27-4fbe-9e0b-b0057c52d6a6"
}`,
			OperationName: "GetAlternativeDishes",
			Created:       time.Now(),
			Tags:          tags,
			Remark:        "",
		}
		if err := logger.Mgr.PutLog(&logData); err != nil {
			t.Error(err)
		}
	}
}
