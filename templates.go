package logger

import "time"

type Template = int

const (
	RESTful Template = iota
	Graphql
)

// RESTful上傳紀錄模板
type RESTfulService struct {
	Service string    `json:"service"`
	IP      string    `json:"request_ip" default:"192.168.1.1"`
	Status  string    `json:"status"`
	Method  string    `json:"method"`
	Path    string    `json:"path"`
	Created time.Time `json:"created,omitempty"`
	Tags    []string  `json:"tags,omitempty"`
	Remark  string    `json:"remark,omitempty"`
}

// Graphql上傳紀錄模板
type GraphqlService struct {
	IP            string      `json:"request_ip" default:"192.168.1.1"`
	Request       interface{} `json:"request,omitempty"`
	Query         string      `json:"query,omitempty"`
	Variables     interface{} `json:"variables,omitempty"`
	OperationName string      `json:"operationName,omitempty"`
	Created       time.Time   `json:"created,omitempty"`
	Tags          []string    `json:"tags,omitempty"`
	Remark        string      `json:"remark,omitempty"`
}
