package logger

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/olivere/elastic/v7"
)

var Mgr ILogManager = &logManager{}

type ILogManager interface {
	Init(config *Config) error
	Client() *elastic.Client
	Type() int
	PutLog(data interface{}) error
}

type logManager struct {
	client  *elastic.Client
	cfg     *Config
	mapping string
}

func (lm *logManager) Init(config *Config) error {
	if config == nil {
		log.Fatal("logger manager config can not be nil")
	}

	ctx := context.Background()

	esURL := fmt.Sprintf("%s:%s", config.URL, config.Port)
	log.Printf("elastic url: %s by auth user: %s password: %s", esURL, config.BasicAuthUser, config.BasicAuthPassword)
	client, err := elastic.NewClient(
		elastic.SetURL(esURL),
		elastic.SetBasicAuth(config.BasicAuthUser, config.BasicAuthPassword),
		elastic.SetSniff(false),
		elastic.SetHealthcheckInterval(10*time.Second),
		elastic.SetGzip(true),
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC", log.LstdFlags)),
		elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)),
	)

	if err != nil {
		log.Println("create elastic client error: ", err)
		return err
	}

	info, code, err := client.Ping(esURL).Do(ctx)
	if err != nil {
		log.Println("ping elastic server error: ", err)
		return err
	}
	fmt.Printf("elasticsearch return with code %d and version %s\n", code, info.Version.Number)

	lm.client = client

	switch config.Type {
	case RESTful:
		config.Index = config.Index + "_restful"
	case Graphql:
		config.Index = config.Index + "_graphql"
	}
	lm.cfg = config
	lm.mapping = createELKMapping(config.NumberOfShards, config.NumberOfReplicas, config.Type)
	fmt.Println(lm.mapping)
	return nil
}

func (lm *logManager) Client() *elastic.Client {
	return lm.client
}

func (lm *logManager) Type() int {
	return lm.cfg.Type
}

func (lm *logManager) PutLog(data interface{}) error {
	ctx := context.Background()
	exist, err := lm.client.IndexExists(lm.cfg.Index).Do(ctx)
	if err != nil {
		log.Println("check index exists or not error: ", err)
		return err
	}
	if !exist {
		creteIndex, err := lm.client.CreateIndex(lm.cfg.Index).BodyString(lm.mapping).Do(ctx)
		if err != nil {
			log.Println("create index error: ", err)
			return err
		}
		if !creteIndex.Acknowledged {
			log.Println("not acknowledge error: ", err)
		}
	}

	//put the log to server
	result, err := lm.client.Index().Index(lm.cfg.Index).BodyJson(data).Do(ctx)
	if err != nil {
		log.Println("put log error: ", err)
		return err
	}

	log.Printf("Indexed log %s to index %s, type %s\n", result.Id, result.Index, result.Type)
	return nil
}

func createELKMapping(numOfShards, numOfReplicas, templateType int) string {
	mapping := ""
	switch templateType {
	case RESTful:
		mapping = `{"settings":{"number_of_shards":%d,"number_of_replicas":%d},"mappings":{"properties":{"service":{"type":"keyword"},"request_ip":{"type":"ip"},"status":{"type":"text"},"method":{"type":"text"},"path":{"type":"keyword"},"created":{"type":"date"},"tags":{"type":"keyword"},"remark":{"type":"text"}}}}`
	case Graphql:
		mapping = `{"settings":{"number_of_shards":%d,"number_of_replicas":%d},"mappings":{"properties":{"request_ip":{"type":"ip"},"request":{"type":"keyword"},"query":{"type":"text"},"variables":{"type":"text"},"operationName":{"type":"text"},"created":{"type":"date"},"tags":{"type":"keyword"},"remark":{"type":"text"}}}}`
	}
	return fmt.Sprintf(mapping, numOfShards, numOfReplicas)
}
