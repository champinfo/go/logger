package logger

//Config 設定檔 存放與elk連線的相關設定
type Config struct {
	URL               string   `yaml:"url" json:"url"`
	Port              string   `default:"9200" json:"port" yaml:"port"`
	Type              Template `yaml:"type" json:"type"`
	BasicAuthUser     string   `default:"elastic" json:"basicAuthUser" yaml:"basicAuthUser"`
	BasicAuthPassword string   `json:"basicAuthPassword" yaml:"basicAuthPassword"`
	Index             string   `json:"index" yaml:"index"`
	NumberOfShards    int      `default:"1" json:"numberOfShards" yaml:"numberOfShards"`
	NumberOfReplicas  int      `default:"0" json:"numberOfReplicas" yaml:"numberOfReplicas"`
}
